import axios from 'axios'

export default {
    state: {
        characters: [],
        loader: true
    },
    getters: {
        getCharacters(state) {
            return state.characters
        },
        loader(state) {
            return state.loader
        }
    },
    mutations: {
        updateCharactersState(state, characters) {
            state.characters = characters
        },
        updateLoaderState(state, loader) {
            state.loader = loader
        }
    },
    actions: {
        async createdCharacters({commit}) {
            const result = await axios.get('https://breakingbadapi.com/api/characters/')
            commit('updateLoaderState', false)
            commit('updateCharactersState', result.data)
        },
        async createdCharacter({commit}, char_id) {
            return await axios.get(`https://breakingbadapi.com/api/characters/${char_id}`).then(characters => {
                commit('updateLoaderState', false)
                return characters.data
            })
        },
    }
}
